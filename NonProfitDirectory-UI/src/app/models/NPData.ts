/**
 * Created by saurabh on 16/02/17.
 */
export class SearchResult {


    constructor(name: string,
                ein: string,
                addressLine1: string,
                cityName: string,
                state: string,
                zip: string,
                DescriptionProgramSrvcAccomTxt: string,
                id: string,
                BusinessNameControlTxt: string,
                phoneNum: string,
                website: string,
                description: string,
                ExpenseAmt: string,
                taxYear: string,
                rulingDate: string,
                missionDescription: string,
                returnType: string,
                businessOfficerPersonName: string,
                businessOfficerPersonTitle: string,
                businessOfficerPersonPhone: string,
                businessOfficerPersonSignatureDate: string,
                count: number,
                ratings: number,
                EmployeeCnt: String,
                TotalVolunteersCnt: String,
                PYTotalRevenueAmt: String,
                CYTotalRevenueAmt: String,
                CYTotalFundraisingExpenseAmt: String,
                PYContributionsGrantsAmt: String,
                CYContributionsGrantsAmt: String,
                CYProgramServiceRevenueAmt: String,
                PYProgramServiceRevenueAmt: String,
                PYInvestmentIncomeAmt: String,
                CYInvestmentIncomeAmt: String,
                PYOtherRevenueAmt: String,
                CYOtherRevenueAmt: String,
                PYSalariesCompEmpBnftPaidAmt: String,
                CYSalariesCompEmpBnftPaidAmt: String,
                PYTotalProfFndrsngExpnsAmt: String,
                CYTotalProfFndrsngExpnsAmt: String,
                RevenueAmt: String,
                FormationYr: String,
                ProgSrvcAccomActyGrpExpenseAmt: string,
                ProgSrvcAccomActyGrpRevenueAmt: string,
                ProgSrvcAccomActyGrpDesc: string,) {
    }


}
