import { Component } from '@angular/core';
import { LoadingAnimateService } from 'ng2-loading-animate';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  color = 'praimry';
  mode = 'determinate';
  value = 50;
  title = 'app works!';
}
