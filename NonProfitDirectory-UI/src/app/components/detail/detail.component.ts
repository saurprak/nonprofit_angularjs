import 'rxjs/add/operator/switchMap';
import {Component, OnInit}      from '@angular/core';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Location}               from '@angular/common';
import {SearchResult} from "../../models/NPData";
import {HeroService} from "../../services/directory.service";


@Component({
  moduleId: module.id,
  selector: 'my-hero-detail',
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.css'],

})
export class HeroDetailComponent implements OnInit {
  hero: SearchResult;


  constructor(private heroService: HeroService,
              private route: ActivatedRoute,
              private location: Location
    , private router: Router) {
  }

  ngOnInit(): void {
    /*if (sessionStorage.getItem('token') == null) {
      this.router.navigate(['/login'])
    } else {
      this.route.params
        .switchMap((params: Params) => this.heroService.getOrganization(params['id']))
        .subscribe(hero => this.hero = hero);
    }*/
    this.route.params
        .switchMap((params: Params) => this.heroService.getOrganization(params['id']))
        .subscribe(hero => this.hero = hero);
  }

  goBack(): void {
    this.location.back();
  }
}
