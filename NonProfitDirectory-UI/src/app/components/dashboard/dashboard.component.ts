/**
 * Created by saurabh on 14/02/17.
 */
import {Component, OnInit} from '@angular/core';

import {HeroService} from "../../services/directory.service";
import {Hero} from "../../models/DirectoryData";
import {Router} from "@angular/router";


@Component({
    selector: 'my-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.css']

})
export class DashboardComponent implements OnInit {

    public isCollapsed:boolean = true;

  /*  public collapsed(event: any): void {
        console.log(event);
    }

    public expanded(event: any): void {
        console.log(event);
    }*/

    public bigCurrentPage: number = 1;
    public maxSize: number = 5;
    data: Hero;
    url: string;
    before: number
    after: number
    timeDifference: number
    statecheckBoxSelected: boolean = false
    stateValue: string = ''
    searchvalue: string = ''

    constructor(private heroService: HeroService,
                private router: Router) {
    }

    update(value: string) {
        this.searchvalue = value
        this.before = new Date().getTime()

        this.heroService.searchOrganizationByName(this.searchvalue, 0, 10, this.stateValue)
            .subscribe(heroes => {
                this.data = heroes;
            });
        this.after = new Date().getTime()
        this.timeDifference = (this.after - this.before) / 1000
    }

    getAllOrganization(value: string) {
        this.before = new Date().getTime()

        if (value == '')
            this.heroService.getOrganizations(0, 10, this.stateValue)
                .subscribe(heroes => this.data = heroes);
        this.searchvalue = ''
        this.after = new Date().getTime()
        this.timeDifference = (this.after - this.before) / 1000

    }

    applyFilter(search: string, city: string, state: string) {
        console.log(search)
        console.log(city)
        console.log(state)

    }

    checkbox(value: string) {
        if (this.statecheckBoxSelected == false) {
            this.statecheckBoxSelected = true;
            this.stateValue = value
            this.heroService.searchOrganizationByName(this.searchvalue, 0, 10, this.stateValue)
                .subscribe(heroes => {
                    this.data = heroes;
                });
        }
        else {
            this.statecheckBoxSelected = false;
            this.stateValue = ''
            this.heroService.searchOrganizationByName(this.searchvalue, 0, 10, this.stateValue)
                .subscribe(heroes => {
                    this.data = heroes;
                });

        }
        console.log(this.stateValue)
    }

    ngOnInit(): void {

        /*if (sessionStorage.getItem('token') == null) {
         this.router.navigate(['/login'])
         } else {
         this.before = new Date().getTime()

         this.heroService.getOrganizations(0, 10)
         .subscribe(heroes => this.data = heroes);

         this.after = new Date().getTime()
         this.url = 'http://google.com'

         this.timeDifference = (this.after - this.before) / 1000
         }*/
        this.before = new Date().getTime()

        this.heroService.searchOrganizationByName(this.searchvalue, 0, 10, this.stateValue)
            .subscribe(heroes => {
                this.data = heroes;
            });


        this.after = new Date().getTime()
        this.url = 'http://google.com'

        this.timeDifference = (this.after - this.before) / 1000


    }

    public pageChanged(event: any): void {
        /*  if (sessionStorage.getItem('token') == null) {
         this.router.navigate(['/login'])
         } else {
         this.before = new Date().getTime()

         this.heroService.getOrganizations(event.page * 10, 10)
         .subscribe(heroes => this.data = heroes);
         this.after = new Date().getTime()

         this.timeDifference = (this.after - this.before) / 1000
         console.log(this.timeDifference)
         }*/
        this.before = new Date().getTime()
        this.heroService.searchOrganizationByName(this.searchvalue, event.page * 10, 10, this.stateValue)
            .subscribe(heroes => {
                this.data = heroes;
            });

        /*
         this.heroService.getOrganizations(event.page * 10, 10, this.stateValue)
         .subscribe(heroes => this.data = heroes);*/
        this.after = new Date().getTime()

        this.timeDifference = (this.after - this.before) / 1000
        console.log(this.timeDifference)
    }


    public collapsed(event: any): void {
        console.log(event);
    }

    public expanded(event: any): void {
        console.log(event);
    }
}

