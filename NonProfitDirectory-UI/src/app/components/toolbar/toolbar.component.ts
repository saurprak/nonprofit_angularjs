/**
 * Created by saurabh on 20/02/17.
 */
import {Component, Output, EventEmitter} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'toolbar',
  styleUrls: ['toolbar.component.css'],

  templateUrl: 'toolbar.component.html',

  inputs: ['title']
})

export class ToolBarComponent {
  opened: Boolean = false;

  constructor(private router: Router) {
  }

  toggle() {
    this.opened = !this.opened;
  }

  logout() {
    console.log('logout')
    sessionStorage.removeItem('token')
    this.router.navigate(['/login'])
  }
}

