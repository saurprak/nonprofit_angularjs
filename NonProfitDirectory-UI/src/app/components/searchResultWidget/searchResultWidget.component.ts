/**
 * Created by saurabh on 14/02/17.
 */
import {Component, OnInit, Input} from '@angular/core';
import {SearchResult} from "../../models/NPData";


@Component({
    selector: 'search-result',
    templateUrl: 'searchResultWidget.component.html',
    styleUrls: ['searchResultWidget.component.css']

})
export class SearchResultWidget {


    @Input()
    heroes: SearchResult[] = [];

    public max: number = 10;
    public rate: number = 7;
    public isReadonly: boolean = true;

    constructor() {
        this.loadScript()
    }

    public loadScript() {
        let nodeHead = document.createElement('script');
        nodeHead.innerHTML = "fbq('track', 'Search');"
        document.getElementsByTagName('head')[0].appendChild(nodeHead);
    }


}

