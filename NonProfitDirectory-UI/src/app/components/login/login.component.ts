/**
 * Created by saurabh on 08/03/17.
 */
import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {LoginService} from "../../services/login.service";


@Component({
  selector: 'login',
  templateUrl: 'login.component.html'
})

export class LoginComponent implements OnInit {
  model: any = {};
  loading = false;
  returnUrl: string;
  showAlert: boolean;
  alertMessage: string;

  constructor(private route: ActivatedRoute,
              private router: Router, private loginService: LoginService) {
  }

  ngOnInit() {
    // reset login status

    // get return url from route parameters or default to '/'
  }

  login() {
    //console.log(this.model.username)
    this.loginService.authenticate(this.model.username, this.model.password).subscribe(data => {
      this.returnUrl = data['token']
      if (data['success']) {
        sessionStorage.setItem('token', this.returnUrl)
        this.router.navigate(['/dashboard'])

      } else {
        this.showAlert = true
        this.alertMessage = data['message']
      }
    })
  }
}
