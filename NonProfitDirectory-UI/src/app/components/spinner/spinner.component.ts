/**
 * Created by saurabh on 22/02/17.
 */
'use strict';
import {Component, Input, OnDestroy} from '@angular/core';
@Component({
  selector: 'loading-indicator',
  template: `
    <div [hidden]="!isDelayedRunning" class="spinner">
    <div class="double-bounce1"></div>
    <div class="double-bounce2"></div>
    </div>`,
  styles: [require('./spinner.style.css').toString()]
})

export class SpinnerComponent implements OnDestroy {
  private currentTimeout: number;
  private isDelayedRunning: boolean = false;
  @Input()
  public delay: number = 300;
  @Input()
  public set isRunning(value: boolean) {
    if (!value) {
      this.cancelTimeout();
      this.isDelayedRunning = false;
      return;
    }

    if (this.currentTimeout) {
      return;
    }

    this.currentTimeout = window.setTimeout(() => {
      this.isDelayedRunning = value;
      this.cancelTimeout();
    }, this.delay);
  }

  private cancelTimeout(): void {
    clearTimeout(this.currentTimeout);
    this.currentTimeout = undefined;
  }

  ngOnDestroy(): void {
    this.cancelTimeout();
  }
}
