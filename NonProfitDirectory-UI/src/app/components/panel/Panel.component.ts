/**
 * Created by saurabh on 20/02/17.
 */
import {Component, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'panel',
  styleUrls: ['panel.component.css'],

  templateUrl: 'panel.component.html',

  inputs: ['title']
})

export class PanelComponent {
  opened: Boolean = false;

  toggle() {
    this.opened = !this.opened;
  }
}

