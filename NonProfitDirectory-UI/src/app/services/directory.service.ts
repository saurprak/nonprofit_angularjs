import {Injectable}    from '@angular/core';
import {Headers, Http, Response, RequestOptions} from '@angular/http';
import {Observable} from "rxjs";
import {SearchResult} from "../models/NPData";
import {Hero} from "../models/DirectoryData";
@Injectable()
export class HeroService {
    //HOST: string = 'localhost'
    HOST: string = '52.43.125.238'

    constructor(private http: Http) {
    }


    getOrganizations(offset: number, size: number, state: string): Observable<Hero> {
        let headers = new Headers({'Accept': 'application/json'});
        headers.append('Authorization', sessionStorage.getItem('token'));

        let options = new RequestOptions({headers: headers});

        return this.http.post('http://' + this.HOST + ':3000/api/organizations990', {
            "offset": offset,
            "size": size,
            "state": state
        }, options).map((response: Response) => <Hero>response.json()).catch(this.handleError);


    }

    searchOrganizationByName(searchValue: string, offset: number, size: number, state: string): Observable<Hero> {
        let headers = new Headers({'Accept': 'application/json'});
        headers.append('Authorization', sessionStorage.getItem('token'));

        let options = new RequestOptions({headers: headers});
        return this.http.post('http://' + this.HOST + ':3000/api/searchByName/', {
            "offset": offset,
            "size": size,
            "state": state,
            "name": searchValue
        }, options).map((response: Response) => <Hero>response.json()).catch(this.handleError);


    }

    getOrganization(id: String): Observable<SearchResult> {
        let headers = new Headers({'Accept': 'application/json'});
        headers.append('Authorization', sessionStorage.getItem('token'));

        let options = new RequestOptions({headers: headers});
        return this.http.get('http://' + this.HOST + ':3000/api/organization990/' + id, options).map((response: Response) => <SearchResult>response.json()).catch(this.handleError);

    }

    getOrganizationCount(): Observable<number> {
        return this.http.get('http://' + this.HOST + ':3000/api/organizationCount').map((response: Response) => <number>response.json()).catch(this.handleError);

    }

    testService(): Observable<Hero> {
        return this.http.get('http://' + this.HOST + ':3000/api/test').map((response: Response) => <Hero>response.json()).catch(this.handleError);

    }


    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}
