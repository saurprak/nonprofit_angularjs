/**
 * Created by saurabh on 08/03/17.
 */
import {Injectable}    from '@angular/core';
import {Headers, Http, Response} from '@angular/http';
import {Observable} from "rxjs";
import {SearchResult} from "../models/NPData";
import {Hero} from "../models/DirectoryData";
import {LoginData} from "../models/loginData";
@Injectable()
export class LoginService {
  constructor(private http: Http) {
  }

  authenticate(username: string, password: string): Observable<LoginData> {

    return this.http.post('http://54.68.117.70:3000/authenticate', {
      "email": username,
      "password": password
    }).map((response: Response) => <LoginData>response.json()).catch(this.handleError);


  }


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
