import {NgModule}      from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule}   from '@angular/forms';
import {HttpModule}    from '@angular/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent}         from './app.component';
import {HeroService} from "./services/directory.service";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {HeroDetailComponent} from "./components/detail/detail.component";
import {SearchResultWidget} from "./components/searchResultWidget/searchResultWidget.component";
import {MaterialModule, MdSliderModule} from "@angular/material";
import {PanelComponent} from "./components/panel/Panel.component";
import {TabsModule, PaginationModule, CollapseModule} from "ng2-bootstrap";
import {ToolBarComponent} from "./components/toolbar/toolbar.component";
import {SpinnerComponent} from "./components/spinner/spinner.component";

import {RatingModule} from 'ng2-bootstrap';
import {LoginComponent} from "./components/login/login.component";
import {LoginService} from "./services/login.service";
import {DropdownModule} from 'ng2-bootstrap';
import {AlertModule} from 'ng2-bootstrap';


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        MaterialModule.forRoot(),
        TabsModule.forRoot(),
        PaginationModule.forRoot(),
        AppRoutingModule,
        CollapseModule.forRoot(),
        RatingModule.forRoot(),
        DropdownModule.forRoot(),
        AlertModule.forRoot(),
        DropdownModule.forRoot()



    ],
    declarations: [
        AppComponent,
        DashboardComponent,
        HeroDetailComponent,
        SearchResultWidget,
        PanelComponent,
        ToolBarComponent,
        SpinnerComponent,
        LoginComponent
    ],
    providers: [HeroService, LoginService],

    bootstrap: [AppComponent]

})
export class AppModule {
}
