import { NonProfitDirectoryUIPage } from './app.po';

describe('non-profit-directory-ui App', function() {
  let page: NonProfitDirectoryUIPage;

  beforeEach(() => {
    page = new NonProfitDirectoryUIPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
